package frc.robot.commands.Swerve;

import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.Constants;
import frc.robot.subsystems.SwerveSubsystem;
import java.util.function.Supplier;


/**
 * Creates a command that takes xbox controller joystick input to drive a swerve base
 *
 * @author Phillip Lai
 */
public class SwerveJoystickCommand extends CommandBase {

    private final SwerveSubsystem swerveSubsystem;
    private final Supplier<Double> xSpdFunction, ySpdFunction, turningSpdFunction;
    private final Supplier<Trigger> robotOrientedFunction, fastFunction, slowFunction;
    private final SlewRateLimiter xLimiter, yLimiter, turningLimiter;

    public SwerveJoystickCommand(
            SwerveSubsystem swerveSubsystem,
            Supplier<Double> vXSupplier,
            Supplier<Double> vYSupplier,
            Supplier<Double> vThetaSupplier,
            Supplier<Boolean> fieldRelativeSupplier,
            boolean isOpenLoop
            ) 
        {
        
    }

    @Override
    public void execute() {
        // 1. Real-time joystick inputs
        

        // 2. Apply deadband
        

        // 3. Make driving smoother
        

        // 4. send the speeds off to the SwerveSubsystem to be dealt with

       
    }

    @Override
    public void end(boolean interrupted) {
        swerveSubsystem.stopModules();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
