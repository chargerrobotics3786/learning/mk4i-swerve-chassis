// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.subsystems.SwerveSubsystem;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {
    // The robot's subsystems and commands are defined here...

    // Subsystems
    private final SwerveSubsystem swerveSubsystem = new SwerveSubsystem();

    // Command Groups
    
    // Swerve

    // Sets different button bindings for test mode
    public void initBindings(boolean isTest) {
        unbindBindings();
        if (isTest) {
            configureTestBindings();
        } else {
            configureBindings();
        }
    }

    private void configureBindings() {
        
    }

    private void configureTestBindings() {
        
    }

    /** clears all controller bindings */
    private void unbindBindings() {
        CommandScheduler.getInstance().getActiveButtonLoop().clear();
    }

    public void enableSubsystems() {
        
    }

    public void disableSubsystems() {
        
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() { // should be Command type method
        return null;
    }
}